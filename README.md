# This is a souped up version of the tutorials found here:
* http://thenewcode.com/977/Create-Interactive-HTML5-Video-with-WebVTT-Chapters
* https://hacks.mozilla.org/2014/08/building-interactive-html5-videos/

The problem with the TheNewCode.com one is that it's written only to work on one video on a whole website. The Mozilla one is putting its examples in a weird iFrame and I didn't feel like digging into it to see all of their examples.

The code in this repo is designed to work across all videos on a website (so long as they're in a figure element) and for there to be multiple videos on the same page. It even takes into account that if you have multiple videos on the same page and two or more happen to have the same start time, it'll know which one you care about and target that video.

The files give more info about all the nuances, especially the JS file.
